package com.epam;

import com.epam.model.enums.City;
import com.epam.model.enums.PizzaType;
import com.epam.model.factory.Store;
import com.epam.model.factory.impl.PizzaStore;
import com.epam.model.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/orders/*")
public class OrderServlet extends HttpServlet {
    private HashMap<Integer, Pizza> orders;
    private Store store;
    private static Logger logger = LogManager.getLogger(OrderServlet.class);

    @Override
    public void init() throws ServletException {
        orders = new HashMap<>();
        store = new PizzaStore();
        logger.info("Servlet" + this.getServletName() + "has started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info(this.getServletName() + " --> doGet()");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Page Title</title>\n"
                + "</head>\n"
                + "<body>");
        out.println("<p><a href='orders'>REFRESH</a></p>");
        out.println(getOrderForm());
        out.println(getDeleteForm());
        //ordered pizzas list
        out.println("<h2>Ordered pizzas:</h2>");
        for (Map.Entry<Integer, Pizza> entry : orders.entrySet()) {
            out.println("<div style='background-color:lightgrey; border: black solid 2px;\n"
                    + "padding: 10px 25px; margin: 10px'>\n"
                    + "<p style=' font-weight: bold; color: darkred'> " + entry.getValue().getName() + "</p>\n"
                    + "<p style=' font-weight: bold; color: darkred'>Id: " + entry.getKey() + "</p>\n"
                    + "<p>City: " + entry.getValue().getCity() + "</p>\n"
                    + "<p>Toppings: " + entry.getValue().getToppings() + "</p>\n"
                    + "<p>Dough type: " + entry.getValue().getDoughType() + "</p>\n"
                    + "<p>Sauce type: " + entry.getValue().getSauceType() + "</p>\n"
                    + "</div>");
        }
        out.println("</body>\n"
                + "</html>");
    }

    private String getOrderForm() {
        return "<div style='background-color:lightgreen; border: black solid 2px;\n"
                + "padding: 10px 25px; margin: 10px; width:30%'>\n"
                + "<p><b>ORDER PIZZA</b></p>"
                + "<form action='orders' method='POST'>\n"
                + "<div style='display:inline-block; margin-right:20px'>\n"
                + "<p>City:</p>\n"
                + "<input type='radio' name='city_name' value='Lviv'>Lviv<br>\n"
                + "<input type='radio' name='city_name' value='Kyiv'>Kyiv<br>\n"
                + "<input type='radio' name='city_name' value='Dnipro'>Dnipro<br>\n"
                + "</div>\n"
                + "<div style='display:inline-block; margin-right:20px'>\n"
                + "<p>Pizza type:</p>\n"
                + "<input type='radio' name='pizza_type' value='cheese'>Cheese<br>\n"
                + "<input type='radio' name='pizza_type' value='pepperoni'>Pepperoni<br>\n"
                + "<input type='radio' name='pizza_type' value='veggie'>Veggie<br>\n"
                + "<input type='radio' name='pizza_type' value='clam'>Clam<br>\n"
                + "</div>\n"
                + "<button style='padding:10px' type='submit'>Order</button>\n"
                + "</form>\n"
                + "</div>";
    }

    private String getDeleteForm() {
        //delete form
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<div style='background-color:yellow; border: black solid 2px;\n"
                + "padding: 10px 25px; margin: 10px; width:30%'>\n"
                + "<form>\n"
                + "<p><b>DELETE PIZZA</b></p>\n"
                + "<div style='display:inline-block; margin-right:20px'>\n"
                + "<select required multiple name='pizza_id' style='min-width:40px'>");
        for (Map.Entry<Integer, Pizza> entry : orders.entrySet()) {
            stringBuilder.append("<option value='" + entry.getKey() + "'>" + entry.getKey() + "</option>");
        }
        stringBuilder.append("</select>\n"
                + "</div>\n"
                + "<button style='padding:10px' onclick='remove(this.form.pizza_id.value)'>Delete</button>\n"
                + "</form>\n"
                + "</div>");
        stringBuilder.append("<script type='text/javascript'>\n"
                + "function remove(id) {fetch('orders/' + id, {method: 'DELETE'});}\n"
                + "</script>");
        return stringBuilder.toString();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info(this.getServletName() + " --> doPost()");
        City city = null;
        PizzaType pizzaType = null;
        String cityName = req.getParameter("city_name");
        String pizzaTypeName = req.getParameter("pizza_type");
        try {
            switch (cityName) {
                case "Lviv":
                    city = City.Lviv;
                    break;
                case "Kyiv":
                    city = City.Kyiv;
                    break;
                case "Dnipro":
                    city = City.Dnipro;
                    break;
            }
            switch (pizzaTypeName) {
                case "cheese":
                    pizzaType = PizzaType.Cheese;
                    break;
                case "pepperoni":
                    pizzaType = PizzaType.Pepperoni;
                    break;
                case "veggie":
                    pizzaType = PizzaType.Veggie;
                    break;
                case "clam":
                    pizzaType = PizzaType.Clam;
                    break;
            }
            logger.info("New pizza --> city=" + city + ", type=" + pizzaType);
            Pizza pizza = store.order(city, pizzaType);
            orders.put(pizza.getId(), pizza);
        } catch (Exception e) {
            logger.error("Response status = 400");
            resp.setStatus(400);
        }
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info(this.getServletName() + " --> doDelete()");
        String id = req.getRequestURI();
        id = id.replace("/task16_servlets-1.0-SNAPSHOT/orders/", "");
        orders.remove(Integer.parseInt(id));
        logger.info("Deleted pizza --> id=" + id);
    }
}
