package com.epam.model.factory;

import com.epam.model.enums.City;
import com.epam.model.enums.PizzaType;
import com.epam.model.pizza.Pizza;

public abstract class Store {

    protected abstract Pizza createPizza(PizzaType pizzaType);

    public Pizza order(City city, PizzaType pizzaType) {
        Pizza pizza = createPizza(pizzaType);
        pizza.prepare(city);
        return pizza;
    }
}
