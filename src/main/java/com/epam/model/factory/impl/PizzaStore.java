package com.epam.model.factory.impl;

import com.epam.model.enums.PizzaType;
import com.epam.model.factory.Store;
import com.epam.model.pizza.Pizza;
import com.epam.model.pizza.impl.CheesePizza;
import com.epam.model.pizza.impl.ClamPizza;
import com.epam.model.pizza.impl.PepperoniPizza;
import com.epam.model.pizza.impl.VeggiePizza;

public class PizzaStore extends Store {

    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        Pizza pizza = null;
        if (pizzaType == PizzaType.Cheese) {
            pizza = new CheesePizza();
        } else if (pizzaType == PizzaType.Veggie) {
            pizza = new VeggiePizza();
        } else if (pizzaType == PizzaType.Clam) {
            pizza = new ClamPizza();
        } else if (pizzaType == PizzaType.Pepperoni) {
            pizza = new PepperoniPizza();
        }
        return pizza;
    }
}
