package com.epam.model.enums;

public enum PizzaType {
    Cheese, Veggie, Clam, Pepperoni
}
