package com.epam.model.enums;

public enum SauceType {
    Marinara, PlumTomato, Pesto
}
