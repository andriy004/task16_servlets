package com.epam.model.enums;

public enum DoughType {
    Thick, Thin
}
