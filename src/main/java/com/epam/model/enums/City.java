package com.epam.model.enums;

public enum City {
    Lviv("Lviv"), Kyiv("Kyiv"), Dnipro("Dnipro");

    private final String NAME;

    City(String name) {
        NAME = name;
    }

    public String getName() {
        return NAME;
    }
}
