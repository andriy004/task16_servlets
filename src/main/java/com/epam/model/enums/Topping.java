package com.epam.model.enums;

public enum Topping {
    Mozzarella, Parmesan, Cheddar,
    Sausage, Ham, Pepperoni,
    Tomatoes, Pepper, Cucumbers, Olives, Onion,
    Mushrooms, Clams
}
