package com.epam.model.pizza;


import com.epam.model.enums.City;
import com.epam.model.enums.DoughType;
import com.epam.model.enums.SauceType;
import com.epam.model.enums.Topping;

import java.util.List;

public interface Pizza {

    void prepare(City city);

    int getId();

    String getName();

    String getCity();

    List<Topping> getToppings();

    DoughType getDoughType();

    SauceType getSauceType();
}
