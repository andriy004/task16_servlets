package com.epam.model.pizza.impl;

import com.epam.model.enums.City;
import com.epam.model.enums.DoughType;
import com.epam.model.enums.SauceType;
import com.epam.model.enums.Topping;

public class PepperoniPizza extends AbstractPizza{

    public PepperoniPizza() {
        super("PepperoniPizza");
    }

    @Override
    public void prepare(City city) {
        if (city == City.Lviv) {
            toppings.add(Topping.Cheddar);
            toppings.add(Topping.Pepperoni);
            toppings.add(Topping.Mushrooms);
            toppings.add(Topping.Olives);
            toppings.add(Topping.Pepper);
            doughType = DoughType.Thick;
            sauceType = SauceType.Marinara;
            this.city = city.getName();
        } else if (city == City.Kyiv) {
            toppings.add(Topping.Parmesan);
            toppings.add(Topping.Pepperoni);
            toppings.add(Topping.Tomatoes);
            toppings.add(Topping.Olives);
            doughType = DoughType.Thick;
            sauceType = SauceType.PlumTomato;
            this.city = city.getName();
        } else if (city == City.Dnipro) {
            toppings.add(Topping.Mozzarella);
            toppings.add(Topping.Pepperoni);
            toppings.add(Topping.Mushrooms);
            toppings.add(Topping.Onion);
            toppings.add(Topping.Tomatoes);
            doughType = DoughType.Thin;
            sauceType = SauceType.Pesto;
            this.city = city.getName();
        }
        logger.trace("The pizza '" + name + "' from " + this.city + " has been prepared!");
    }
}
