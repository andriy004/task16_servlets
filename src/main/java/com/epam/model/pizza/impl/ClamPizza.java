package com.epam.model.pizza.impl;

import com.epam.model.enums.City;
import com.epam.model.enums.DoughType;
import com.epam.model.enums.SauceType;
import com.epam.model.enums.Topping;

public class ClamPizza extends AbstractPizza{

    public ClamPizza() {
        super("ClamPizza");
    }

    @Override
    public void prepare(City city) {
        if (city == City.Lviv) {
            toppings.add(Topping.Cheddar);
            toppings.add(Topping.Ham);
            toppings.add(Topping.Clams);
            toppings.add(Topping.Mushrooms);
            toppings.add(Topping.Tomatoes);
            toppings.add(Topping.Pepper);
            doughType = DoughType.Thin;
            sauceType = SauceType.Pesto;
            this.city = city.getName();
        } else if (city == City.Kyiv) {
            toppings.add(Topping.Parmesan);
            toppings.add(Topping.Sausage);
            toppings.add(Topping.Clams);
            toppings.add(Topping.Tomatoes);
            toppings.add(Topping.Olives);
            doughType = DoughType.Thin;
            sauceType = SauceType.PlumTomato;
            this.city = city.getName();
        } else if (city == City.Dnipro) {
            toppings.add(Topping.Mozzarella);
            toppings.add(Topping.Ham);
            toppings.add(Topping.Clams);
            toppings.add(Topping.Mushrooms);
            toppings.add(Topping.Onion);
            toppings.add(Topping.Tomatoes);
            doughType = DoughType.Thick;
            sauceType = SauceType.Marinara;
            this.city = city.getName();
        }
        logger.trace("The pizza '" + name + "' from " + this.city + " has been prepared!");
    }
}
