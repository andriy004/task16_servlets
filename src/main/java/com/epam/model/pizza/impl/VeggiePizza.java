package com.epam.model.pizza.impl;

import com.epam.model.enums.City;
import com.epam.model.enums.DoughType;
import com.epam.model.enums.SauceType;
import com.epam.model.enums.Topping;

public class VeggiePizza extends AbstractPizza {

    public VeggiePizza() {
        super("VeggiePizza");
    }

    @Override
    public void prepare(City city) {
        if (city == City.Lviv) {
            toppings.add(Topping.Mozzarella);
            toppings.add(Topping.Mushrooms);
            toppings.add(Topping.Tomatoes);
            toppings.add(Topping.Pepper);
            doughType = DoughType.Thin;
            sauceType = SauceType.PlumTomato;
            this.city = city.getName();
        } else if (city == City.Kyiv) {
            toppings.add(Topping.Cheddar);
            toppings.add(Topping.Tomatoes);
            toppings.add(Topping.Onion);
            doughType = DoughType.Thin;
            sauceType = SauceType.Marinara;
            this.city = city.getName();
        } else if (city == City.Dnipro) {
            toppings.add(Topping.Parmesan);
            toppings.add(Topping.Mushrooms);
            toppings.add(Topping.Cucumbers);
            toppings.add(Topping.Onion);
            doughType = DoughType.Thick;
            sauceType = SauceType.Pesto;
            this.city = city.getName();
        }
        logger.trace("The pizza '" + name + "' from " + this.city + " has been prepared!");
    }
}
