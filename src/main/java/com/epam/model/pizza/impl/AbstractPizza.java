package com.epam.model.pizza.impl;

import com.epam.model.enums.City;
import com.epam.model.enums.DoughType;
import com.epam.model.enums.SauceType;
import com.epam.model.enums.Topping;
import com.epam.model.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPizza implements Pizza {

    private static int unique = 1;
    private int id;
    protected String name;
    protected String city;
    protected List<Topping> toppings;
    protected DoughType doughType;
    protected SauceType sauceType;
    protected Logger logger;

    public AbstractPizza(String name) {
        id = unique++;
        this.name = name;
        logger = LogManager.getLogger(AbstractPizza.class);
        toppings = new ArrayList<>();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public abstract void prepare(City city);

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public List<Topping> getToppings() {
        return toppings;
    }

    @Override
    public DoughType getDoughType() {
        return doughType;
    }

    @Override
    public SauceType getSauceType() {
        return sauceType;
    }
}
